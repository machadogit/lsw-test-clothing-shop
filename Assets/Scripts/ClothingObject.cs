﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Clothing Object")]
public class ClothingObject : ScriptableObject
{
    public Sprite main_image;
    public Sprite icon_image;
    public string clothing_name;
    public string discription;
    public int stockCount;
    public float price;
}
