﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableShop : MonoBehaviour
{
    public enum interactable_states
    {
        AWAY,
        DETECTED,
        INTERACTING
    };
    public interactable_states myState;

    private CircleCollider2D myCollider;
    private Animator myAnimator;
    private bool pressedInteraction = false;

    [Tooltip("List of available clothes that will be prompted at shop ui's")]
    public ClothingObject[] Available_Clothes;

    [Tooltip("UI Shop script reference")]
    public UIShop UIShop;

    void Start()
    {
        myCollider = GetComponent<CircleCollider2D>();

        myAnimator = GetComponent<Animator>();

        if(Available_Clothes == null)
            Available_Clothes = new ClothingObject[0];

        SetAway();
    }

    void FixedUpdate()
    {
        checkInteractionButton();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            if(myState != interactable_states.INTERACTING)
                SetDetected();
        }
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if(other.tag == "Player")
        {
            SetAway();
        }
    }

    public void SetDetected()
    {
        myState = interactable_states.DETECTED;

        myAnimator.SetBool("isDetected", true);
        myAnimator.SetBool("isInteracting", false);

        if(UIShop && UIShop.currentState != UIShop.UI_states.CLOSED)
        {
            if(UIShop.currentState != UIShop.UI_states.IDLE)
                Available_Clothes = UIShop.CloseUI();
            else
                UIShop.CloseUI();
        }
    }

    public void SetInteracting()
    {
        myState = interactable_states.INTERACTING;

        myAnimator.SetBool("isInteracting", true);

        if(UIShop)
        {
            UIShop.gameObject.SetActive(true);
            UIShop.OpenUI(Available_Clothes);
        }
    }

    public void SetAway()
    {
        myState = interactable_states.AWAY;

        myAnimator.SetBool("isInteracting", false);
        myAnimator.SetBool("isDetected", false);

        if(UIShop && UIShop.currentState != UIShop.UI_states.CLOSED)
        {
            if(UIShop.currentState != UIShop.UI_states.IDLE)
                Available_Clothes = UIShop.CloseUI();
            else
                UIShop.CloseUI();
        }
    }

    private void checkInteractionButton()
    {
        if(Input.GetAxis("Space") > 0)
        {
            if(!pressedInteraction)
            {
                if(myState == interactable_states.DETECTED)
                {
                    SetInteracting();
                }else if(myState == interactable_states.INTERACTING)
                {
                    SetDetected();
                }

                pressedInteraction = true;
            }

        }else
        {
            pressedInteraction = false;
        }
    }
}
