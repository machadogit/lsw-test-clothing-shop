﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShop : MonoBehaviour
{
    public enum UI_states
    {
        IDLE,
        OPENED,
        CLOSED
    }
    public UI_states currentState;
    public string shop_title;
    public GameObject showCase;

    private ClothingObject[] current_clothes;
    private Animator myAnimator;

    public Vector3 initialPositionOffset;
    public Transform player_transform;

    public GameObject clothingButton_prefab;

    public int clothesPerPage;

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = GetComponent<Animator>();

        //setup_UI();
    }

    void FixedUpdate() {
        
        transform.position = player_transform.position + initialPositionOffset;
    }

    public void OpenUI(ClothingObject[] available_clothes)
    {
        if(currentState == UI_states.OPENED)return;

        currentState = UI_states.OPENED;

        current_clothes = available_clothes;

        ActivateUI();
    }

    public ClothingObject[] CloseUI()
    {
        if(currentState == UI_states.CLOSED) return current_clothes; 

        currentState = UI_states.CLOSED;

        DeactivateUI();

        return current_clothes;
    }

    private void setup_UI()
    {
        GetComponentInChildren<Text>().text = shop_title;

        setupPage(1);
    }

    private void ActivateUI()
    {
        myAnimator.SetBool("isOpen", true);

        Invoke("setup_UI", .1f);
    }

    private void DeactivateUI()
    {
        myAnimator.SetBool("isOpen", false);

        deletePage();
    }

    public void display_clothing(ClothingObject clothing_obj)
    {
        Text[] texts = showCase.GetComponentsInChildren<Text>();

        texts[0].text = clothing_obj.clothing_name;
        texts[1].text = clothing_obj.discription;
        texts[2].text = "*$ " + clothing_obj.price.ToString();
        texts[3].text = "In stock - " + clothing_obj.stockCount;

        Image img = showCase.GetComponentInChildren<Image>();

        img.sprite = clothing_obj.main_image;

        Button buyButton = GetComponentInChildren<Button>();

    }

    public void setupPage(int pageIndex)
    {
        ClothingObject[] pageObjects = new ClothingObject[clothesPerPage];

        current_clothes.CopyTo(pageObjects, (pageIndex - 1) * clothesPerPage);

        for(int i=0; i<pageObjects.Length; i++)
        {
            if(pageObjects[i] == null) return;

            Vector3 newPosition = player_transform.position + initialPositionOffset;
            newPosition.x += .035f;
            newPosition.y -= .015f * i - .02f;

            GameObject bClone = (GameObject) Instantiate(clothingButton_prefab, newPosition, 
                transform.rotation, transform.GetComponentsInChildren<Transform>()[1]);

            bClone.GetComponentInChildren<Text>().text = pageObjects[i].clothing_name;

            ClothingObject showObjs = pageObjects[i];

            Button cloneButton = bClone.GetComponent<Button>();
            cloneButton.onClick.AddListener(delegate
                {
                    display_clothing(showObjs);
                }
            );
        }
    }

    public void deletePage()
    {
        Button[] uiButtons = GetComponentsInChildren<Button>();

        for(int i=0; i<uiButtons.Length; i++)
        {
            if(uiButtons[i].name != "Buy_button")
                Destroy(uiButtons[i].gameObject);
        }
    }
}
