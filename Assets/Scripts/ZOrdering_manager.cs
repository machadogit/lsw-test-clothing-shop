﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZOrdering_manager : MonoBehaviour
{

    public GameObject player;

    private SpriteRenderer mySprite;
    private int initialSortingOrder;

    void Start()
    {
        mySprite = GetComponent<SpriteRenderer>();

        if(mySprite)
            initialSortingOrder = mySprite.sortingOrder;
    }

    void Update()
    {
        if(player != null)
        {
            if(player.transform.position.y > transform.position.y)
            {
                mySprite.sortingOrder = initialSortingOrder + player.GetComponent<SpriteRenderer>().sortingOrder + 1;
            }else
            {
                mySprite.sortingOrder = initialSortingOrder + player.GetComponent<SpriteRenderer>().sortingOrder - 1;
            }
        }
    }

}
